﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//This code taken from Adrian Gordan @ www.returnofthebrain.co.uk
public class ScreenRegion : MonoBehaviour
{
	Rigidbody2D _rb2d;
	BoxCollider2D _collider;
	void Awake()
	{
		_collider = gameObject.GetComponent<BoxCollider2D>();
		_rb2d = gameObject.AddComponent<Rigidbody2D>();
		_rb2d.isKinematic = true;
	}
	void SetNewCameraBounds()
	{
		CameraAlign cam = Camera.main.gameObject.GetComponent<CameraAlign>();
		cam.SetNewBounds(_collider.bounds);
	}
	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.tag == "Player")
		{
			SetNewCameraBounds();
		}
	}
}