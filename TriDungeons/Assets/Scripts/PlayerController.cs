﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
	int numKeys = 0;
	int triggeredCount;
	public float speed;
	Transform pushablePos;
	Transform keyPos;
	GameObject key;
	public Text keyText;
	public Text helpText;
    public GameObject primaryImage;

	private void Update()
	{
		if (Input.GetAxisRaw("Horizontal") > 0.5f || Input.GetAxisRaw("Horizontal") < -0.5f)
		{
			transform.Translate(new Vector3(Input.GetAxisRaw("Horizontal") * speed * Time.deltaTime, 0f, 0f));
		}
		if (Input.GetAxisRaw("Vertical") > 0.5f || Input.GetAxisRaw("Vertical") < -0.5f)
		{
			transform.Translate(new Vector3(0f, Input.GetAxisRaw("Vertical") * speed * Time.deltaTime, 0f));
		}
	}

	private void Start()
	{
		key = GameObject.FindWithTag("Key");
		key.SetActive(false);
        primaryImage = GameObject.FindGameObjectWithTag("PrimaryWep");
        primaryImage.SetActive(false);

		GameObject pushableBlock = GameObject.FindWithTag("PushableSwitch");
		pushablePos = pushableBlock.transform;
	}

	private void OnTriggerEnter2D(Collider2D collision)
	{
		if (collision.tag == "Key")
		{
			if (collision.gameObject.activeSelf)
			{
				collision.gameObject.SetActive(false);
				numKeys++;
				keyText.text = "Keys:" + numKeys;
				helpText.text = "A key! Try moving through the door.";
			}
		}
        else if (collision.tag == "DoorKey")
        {
            if (collision.gameObject.activeSelf)
            {
                collision.gameObject.SetActive(false);
                numKeys++;
                keyText.text = "Keys:" + numKeys;
                helpText.text = "";
            }
        }
        else if (collision.tag == "PushableSwitch")
		{
			if (collision.gameObject.transform.position.x != pushablePos.position.x)
			{
				var key = GameObject.FindWithTag("Key");
				key.SetActive(true);
			}

		}
        else if (collision.tag == "Sword")
        {
            collision.gameObject.SetActive(false);
            primaryImage.SetActive(true);
        }
	}
	private void OnCollisionEnter2D(Collision2D collision)
	{

		if (collision.gameObject.tag == "PushableSwitch")
		{
			if (triggeredCount < 1)
			{
				helpText.text = "Great! But.. the door is locked...";
				key.SetActive(true);
				triggeredCount++;
			}
		}
		else if (collision.gameObject.tag == "LockedDoor")
		{
			if (numKeys > 0)
			{
				helpText.text = "Door unlocked! Theres more to explore...";
				collision.gameObject.SetActive(false);
				numKeys--;
				keyText.text = "Keys:" + numKeys;
			}
		}
        else if (collision.gameObject.name == "LockedDoorMaze")
        {
            if (numKeys > 0)
            {
                collision.gameObject.SetActive(false);
                numKeys--;
                keyText.text = "Keys:" + numKeys;
            }
        }
    }
}
