﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PushPuzzle : MonoBehaviour {
    GameObject door;
	public string pushname;
	// Use this for initialization
	void Awake () {
        if(GameManager.instance == null)
        {
            GameManager.instance = new GameManager();
        }
		door = GameObject.FindGameObjectWithTag("LockDoorPuzzle");
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	private void OnTriggerEnter2D(Collider2D collision)
	{
		if(collision.name == "PuzzleSwitch1")
		{
			GameManager.instance.flag1 = true;
		}
		else if (collision.name == "PuzzleSwitch2")
		{
            GameManager.instance.flag2 = true;
		}
		if(GameManager.instance.flag1 == true && GameManager.instance.flag2 == true)
		{
			door.SetActive(false);
		}
	}
}
